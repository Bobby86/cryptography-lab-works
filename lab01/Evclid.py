# -*- coding: utf-8 -*-
"""
Created on Thu Sep  4 18:56:01 2014

@author: V.Rybnikov IS-16
"""
#import numpy as np

def gcd (a, b):
    """
        Return gcd of a and b arguments
        and variables (x, y) from {a*x + b*y = gcd(a, b)}
        calculated by optimized Evclid algorythm.
        -> return b, x, y
    """

#    try:
#        np.int_(a)  #   cut numeric type value to int type value
#        np.int_(b)
#    except ValueError:
#        pass
    
    if (b > a): 
        a += b
        b = a - b
        a = a - b

    U = [a, 1, 0]
    V = [b, 0, 1]
        
    while V[0]:
        tmp = ( U[0]//V[0] )
        q = tmp 
        T = [ U[0] % V[0] , U[1] - q * V[1] , U[2] - q * V[2] ]
        U = V[:]    # copy
        V = T[:]
        
    return U

####################################

if __name__ == '__main__':

    from BinPow import binPow
    print (gcd(11, 7))
    print (gcd(binPow(10, 9, 5), binPow(10, 6, 5) + 31))
