"""
    Baby step giant step algorythm
    
    @author: V.Rybnikov IS-16
"""

from BinPow import *
from DiffieHellman import *
from math import sqrt
###############################################
def babyGiant (a, p, res):

    m = k = int(sqrt(p)) + 1

    print('m, k =', m, k)
    
    firstSeq =[[res, 0, 0]]
    secondSeq = []

    for item in range(1, m):
        firstSeq.append ([int(((a**item) * res) % p), 0, item])
    print( 'baby', firstSeq )

    for item in range(1, k):
        secondSeq.append ([int((a ** (item * m)) % p), 1, item])
    print('Giant', secondSeq )

    firstSeq += secondSeq

    print('unsorted:', firstSeq)

    firstSeq.sort()

    print('sorted:  ',firstSeq)

    i, j = 0, 0

    for item in range (1, len (firstSeq)):
        
        if (firstSeq[item][0] == firstSeq[item-1][0]):
            if (firstSeq[item][1] != firstSeq[item-1][1]):
                i = firstSeq[item-1][2]
                j = firstSeq[item][2]
                break

    print(i, j)
    x = i * m - j

    return x

###############################################
if ( __name__ == '__main__'):

    a = 2
    P = 23
    res = 9
    x = babyGiant (a, P, res)
    print('x =', x)
    print (binPow (a, x, P))
    
