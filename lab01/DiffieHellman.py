# -*- coding: utf-8 -*-
"""
Created on Thu Sep  4 18:56:01 2014

@author: V.Rybnikov IS-16
"""

from random import randint
from lab01.BinPow import binPow
from lab01.Evclid import gcd

#==============================================================================
# searching for simple numbers
#==============================================================================
def sieveOfEratosthenes (n):
    
    a = list(range(n + 1))
    a[1] = 0
    lst = []
    
    i = 2
    while i <= n:
        if a[i] != 0:
            lst.append(a[i])
            for j in range(i, n+1, i):  #range(start, stop, step)
                a[j] = 0
        i += 1
    
    return lst
#==============================================================================
# check is a number simple or not by Ferma test (for q)
def isSimple (q):
    
    if (2 == q):
        return True
        
    for i in range(99):
        a = randint (1, q-1)
        if ( (1 != gcd (a, q)[0]) or ( 1 != binPow(a, q-1, q))):
            return False
    return True
    
#==============================================================================
def genPublicKeys (maxNum):
    """
        generate p and g for Diffie-Hellman algorythm.
        maxNum is a range where a simple numbers will be founded.
        return p, g (0,0 if failed)
    """
    #get a list of simle numbers from 2 up to last til maxNum
    simpleNums = sieveOfEratosthenes (maxNum)       
    length = len(simpleNums)

    for simple in range(length):
        # just 1 simple number left
        if ( length < 2 ):
            break
        # searching for valid p
        i = randint(0, length-1)     # random index in simpleNums
        p = simpleNums.pop(i)   # get and remove
        length -= 1             # --
        if (p < 7):             # костыль p < 7 is senseless
            continue
        #calculation and validation of q   
        q = int ((p - 1) / 2)
#        print ('length, i, p, q =', length, i , p, q)
        if ( not isSimple(q)):
#            print ('Diffie-Hellman module: q failure')
            continue    
        # generate g
        simpleG = simpleNums[:(i-1)]    # list of all simple numbers lesser than p
        lengthG = len(simpleG)          # ... and length of it
        # searching for valid g
        for G in range(lengthG):
            #
            j = randint (0, lengthG-1)
            g = simpleG.pop(j)
            lengthG -= 1
            # g validation
            if ( 1 != binPow(g, q, p) and (g < p-1)):   
                return p, g
    # failure
    return 0, 0
            
#==============================================================================
class Abonent:
    
    def __init__(self, P, G):

        self.__p = P
        self.__g = G
        self.__secretX = randint(9, 99)
        self.__openY = binPow (G, self.__secretX, P)
    #-----------------------
    def getY (self):
        return self.__openY
    #-----------------------
    def genKey (self, receivedY):
        
        return binPow (receivedY, self.__secretX, self.__p)
    #-----------------------    
#==============================================================================
if __name__ == '__main__':

#    print (sieveOfEratosthenes(1000))    
    
    p, g = genPublicKeys (100)
    
    print ('p, g =', p, g)
    
    if (p and g):
        
        A = Abonent (p, g)
        B = Abonent (p, g)
        
        Ya = A.getY()
        Yb = B.getY()
                
        keyA = A.genKey (Yb)                
        keyB = B.genKey (Ya)

        print('Alice:', keyA)
        print('Bob  :', keyB)

#    print(isSimple (115792089237316195423570985008687907853269984665640564039457584007913129639939))
