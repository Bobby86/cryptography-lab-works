# -*- coding: utf-8 -*-
"""
Created on Thu Sep  4 18:56:01 2014

@author: V.Rybnikov IS-16
"""
        
#####################################
def binPow (a, x, p):
    """
        Return result y of exponentation or ValueError exception
        y = a^x mod p
    """
    # general algorythm
    y = 1
    
    while x:
        if (x & 1):
            y = (y * a) % p
        a = (a**2) % p
        x >>= 1

    return y
######################################
def exponentation (a, x, p):
    
    return a**x % 21
######################################

if __name__ == '__main__':


    import time
    
    t = time.time()
    for i in range(1000000):
        binPow(2, 12, 21)
    t = time.time() - t
    print (t)   
    t = time.time()    
    for i in range(1000000):
        exponentation (2, 12, 21)
    t = time.time() - t
    print (t)

    print(binPow(23,100000,35))
    print( 23 ** 49 % 35 )
