# -*- coding: utf-8 -*-
"""
Created on Wed Dec 10 12:04:37 2014

@author: V.Rybnikov
"""

import sys
import os

PACKAGE_PARENT = '..'
SCRIPT_DIR = os.path.dirname(os.path.realpath(os.path.join(os.getcwd(), os.path.expanduser(__file__))))
sys.path.append(os.path.normpath(os.path.join(SCRIPT_DIR, PACKAGE_PARENT)))

import random
from lab01.BinPow import binPow
from lab05.emoney import *
#################################################
def addToDic (dic, x1, x2):
    if x1 not in dic.keys():
        dic[x1] = [[x2]]
        return True
    else:
        if x2 not in dic[x1][0]:
            dic[x1][0].append (x2)
        return False
    
# v: [vi, vj, ...] <- lv; []
def paint (V, v, curPaint):
    ve = V[v][0]
    try:
        if len(ve) == 2:
            V[v].pop()
            print('###')
    except:
        pass
    paints = []
    p = -1
    for vi in ve:
        p = -1
        try:
            p = V[vi][1]    #get color of v[i]
            paints.append (p)
        except:
            pass
    if len(paints) >= 3:
        return -1
    if not len(paints):
        V[v].append (curPaint)
        return curPaint
    if len(paints) == 1:
        p = (paints[0] + 1) % 3
    else:
        if sum(paints) == 2: p = 1
        elif sum(paints) == 1: p = 2
        else: p = 0
    V[v].append (p)
    return p    
        
class Alice:
    
    def __init__ (self, fileName):
        
        self.__paints = [0, 1, 2]   #R, G, B
        # shuffle paints
        self.__V = {}  # v: paint
        self.__E = []  # v: [vi, vj, ...]
        self.__NdZ = {}
        argFile = open (fileName, 'r')
        #STEP 1
        curPaint = self.__paints[0]
        for line in argFile:
            l = line.split()
            l = [int(x) for x in l]
            self.__E.append(l)

            f = addToDic (self.__V, l[0], l[1])
            if f:
                paint (self.__V, l[0], curPaint)
            f = addToDic (self.__V, l[1], l[0])
            if f:
                paint (self.__V, l[1], curPaint)
            
            curPaint = (curPaint + 1) % 3
            
        paint (self.__V, len(self.__V), curPaint)
            #print (l)
#        print (self.__V)
        argFile.close()

    def getE (self):
        return self.__E
    def getNdZ (self):
        return self.__NdZ
    
    def rGen (self, paint):
        r = random.randint ((1 << 63), (1 << 64) - 1)
        r >>= 2
        r = (r << 2) | paint
        return r

    def step2and3 (self):
        for v in self.__V:
            #step 2
            r = self.rGen (self.__V[v][1])
            self.__V[v].append(r)
            #step 3
            P = genBigP ((1 << 63) + 1, (1 << 64) - 1)
            Q = genBigP ((1 << 63) + 1, (1 << 64) - 1)
            N = P * Q
            c, d = genSecretKeys ((P - 1) * (Q - 1))
            self.__V[v].append ([P, Q, N, c, d])
            #step 4
            Z = binPow (r, d, N)
            self.__V[v].append (Z)
            # Alice have to send to Bob V[v][3][2|4] (N, d) and V[v][4] (Z)
            self.__NdZ[v] = [self.__V[v][3][2], self.__V[v][3][2], self.__V[v][4]] 
        #print (self.__V)

    def step5 (self, edge):
        v0, v1 = edge[0], edge[1]
        res = [self.__V[v0][3][3], self.__V[v1][3][3]]
#        print(res)
        return res # c0, c1

class Bob:

    def __init__ (self, NdZs, Edges):

        self.NdZ = NdZs
        self.E = Edges
        self.curEdge = []

    #step 5
    def getRandEdge (self):
        randEdge = random.randint(0, len(self.E) - 2)  # pair [vi, vj]
        self.curEdge = self.E[randEdge]
        return  self.curEdge

    def stepB5 (self, c0, c1):
        v0 = self.curEdge[0]
        v1 = self.curEdge[1]
        #print ('Bob calculate Z')
        Z0 = binPow (self.NdZ[v0][2], c0, self.NdZ[v0][0]) # Z ^ c mod N
        Z1 = binPow (self.NdZ[v1][2], c1, self.NdZ[v1][0]) # Z ^ c mod N

        p0 = Z0 & 0b11
        p1 = Z1 & 0b11
#        print ('Bob calculate code of colores for each value')
        print (v0, 'color code:', p0)
        print (v1, 'color code:', p1)

        if p0 == p1:
            return False

        return True
#################################################
if __name__ == '__main__':
    
#    vNum = 1000
#    f = open ('ring.txt', 'w')
#    for e in range (1, vNum):
#        f.write (str(e) + ' ' + str(e + 1) + '\n')
#    f.write (str(vNum) + ' ' + str(1))
#    f.close()        
        
    A = Alice('ring.txt')
    A.step2and3 ()          
    E = A.getE ()
    NdZ = A.getNdZ ()

    B = Bob(NdZ, E)

    for i in range (10):
        print()
#        input ('Press Enter for the next step')
#        print()
        edge = B.getRandEdge()
        print ('Bob chose edge: ', edge)
#        print()
#        input ('Press Enter for the next step')
#        print()
        
        c = A.step5 (edge)
        c0, c1 = c[0], c[1]
#        print ('Alice sent to Bob c-keys for this edge')
#        print (c)
#        print()
#        input ('Press Enter for the next step')
#        print()
        
        f = B.stepB5(c0, c1)
        print ('check result: ', end = '')
        if not f:
            print ('Alice a lier!')
            break
        else:
            print ('successfull check...')
    
