# -*- coding: utf-8 -*-
"""
Created on Fri Sep 26 13:18:44 2014

@author: V.Rybnikov
"""

import sys
import os

PACKAGE_PARENT = '..'
SCRIPT_DIR = os.path.dirname(os.path.realpath(os.path.join(os.getcwd(), os.path.expanduser(__file__))))
sys.path.append(os.path.normpath(os.path.join(SCRIPT_DIR, PACKAGE_PARENT)))

import random
from lab01.BinPow import binPow
from lab01.DiffieHellman import *   # <- isSimple,
from lab02.Shamir import isCoprimeInt
#################################################
def genBigP (first, last):

    while True:
        P = random.randint (first, last)
        if isSimple (P):
            return P
#------------------------------------------
def genSecretKeys (P):  # (must be called with P-1 argument here)

    c, d = 0, 0
    while True:
        c = random.randint (2, P - 1)
        d = isCoprimeInt (c, P)
        if d:
            if d < 0: d += P
            return c, d
#-----------------------------------------
def makeADeck (maxCards):
    """
        Generate dictionary where keys are cards and values are random numbers:
        'card' from sorted set of (0..maxCards) values,
        'value' from random set of (100..999) values/
    """
    deck = {}
    for card in range (1, maxCards + 1):
        cardValue = random.randint (100, 999)
        deck[cardValue] = card
#        deck.append ([card, cardValue])
    return deck
#-----------------------------------------
def printAMap (Map):
    print ('value : card')
    for key in Map.keys ():
        print (key, ':', Map[key])
#-----------------------------------------
def printADeck (deck):
#    print ('---deck values:')
    for card in deck:
        print (card)
#-----------------------------------------
def dealTheCards (deck, cardsPerGamer, gamers):
    """
        deal cardsPerGamer random cards from the deck 
        to each gamer from gamers list
        Return True if all cards has been dealt correctly, False otherwise
    """
    length = len (deck)
    try:
        for gamer in gamers:
            for index in range (cardsPerGamer):
                card = deck.pop (random.randint (0, length - 1))
                length -= 1
                gamer.takeACard (card)
    except:
        print ('cannot deal the deck with taken parameters')
        return False
    return True
#################################################
class Gamer:
    
    def __init__ (self, P, name = ''):
        self.__Name = name
        self.__hand = []    # dealt cards in the hand of a player
        self.__P = P
        self.__c, self.__d = genSecretKeys (P - 1)
    #-----------------
    def getP (self): return self.__P
    #-----------------
    def getName (self): return self.__Name
    #-----------------
    def encodeCard (self, card):
        return binPow (card, self.__c, self.__P)
    #-----------------
    def encodeDeck (self, deck):
        print (self.__Name, 'encode and shuffle the deck')
        for card in range (len (deck)):
            deck[card] = binPow (deck[card], self.__c, self.__P)
    #-----------------
    def decodeCard (self, card):
        return binPow (card, self.__d, self.__P)
    #-----------------
    def decodeDeck (self, deck):
        for card in range (len (deck)):
            deck[card] = binPow (deck[card], self.__d, self.__P)
    #-----------------
    def shuffleDeck (self, deck):
        random.shuffle (deck)
    #-----------------
    def takeACard (self, card):
        self.__hand.append (card)
    #-----------------
    def printAHand (self):
        print (self.__Name, ': ')
        printADeck (self.__hand)
    #-----------------
    def getAHand (self):
        return self.__hand
    #-----------------
    def decodeHand (self, Gamers):
        """
            decode this player cards
            take a list of Gamers as 2nd argument
        """        
        for eachCard in range (len (self.__hand)):
            Card = self.__hand.pop () 
            for gamer in Gamers:
                Card = gamer.decodeCard (Card)
            self.__hand.insert (0, Card)    
    #-----------------
    def defineCards (self, Map):
        """
            find the cards for decoded values in the Map 
        """
        print()
        print (self.__Name, 'hand: decoded value : card')
        for card in self.__hand:
            print (card, ':', end='')
            if card in Map.keys():
                card = Map[card]
            else:
                card = str(card) + 'didn\'t found'
            print (card)
#################################################
if __name__ == '__main__':

    P = genBigP ((1 << 48), (1 << 64))
    print ('P =', P)
#    c, d = genSecretKeys (P - 1)
#    print ('c =', c)
#    print ('d =', d)
#    print ((c*d)%(P-1))
    
    print ('A new deck for this game:')
    Map = makeADeck (15)
    printAMap (Map)
    
    # get the cards out
    deck = list (Map.keys ())

    # 5 different gamers:
    A = Gamer (P, 'Alice')
    B = Gamer (P, 'Bob')
    C = Gamer (P, 'Cate')
    D = Gamer (P, 'Dylan')
    E = Gamer (P, 'Eric')
    #   list of gamers:
    Gamers = [A, B, C, D, E]

    #   encode and shuffle the deck by each gamer:
    for gamer in Gamers:
        input ('Press Enter for the next step')
        print ('-----------------------')
        gamer.encodeDeck (deck)
        gamer.shuffleDeck (deck)
        printADeck (deck)
        
    print ('---all cards in the deck has been encoded by every player---')
#    input ('Press Enter for the next step')

    # deal the deck:
    print ('---deal the cards to a players---')
    if not dealTheCards (deck, 2, Gamers):
        sys.exit ()
        
    #   look at what everyone got:
    for gamer in Gamers:
        print()
        gamer.printAHand ()
        input ('Press Enter for the next step')
    print ('rest of cards in the deck: ')
    printADeck (deck)
    print()
    
    #   each gamer decode own hand
    for gamer in Gamers:
        gamer.decodeHand (Gamers)
        
    #   look at what everyone got:
#    print()
#    print ('---After everyone decode their cards: ')
#    for gamer in Gamers:
#        print()
#        gamer.printAHand ()    

    # look for defined cards  
    print ('--every gamer decode his cards and find defined cards for values--')
    for gamer in Gamers:
        gamer.defineCards (Map)
        input ('Press Enter for the next step')
    
        
