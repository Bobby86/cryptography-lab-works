# -*- coding: utf-8 -*-
"""
Created on Fri Oct 24 11:49:46 2014

@author: V.Rybnikov IS-16
"""
import sys
import os

PACKAGE_PARENT = '..'
SCRIPT_DIR = os.path.dirname(os.path.realpath(os.path.join(os.getcwd(), os.path.expanduser(__file__))))
sys.path.append(os.path.normpath(os.path.join(SCRIPT_DIR, PACKAGE_PARENT)))

import random
import hashlib
from lab01.BinPow import binPow
from ctypes import *
import RSA_ds
from lab02.Shamir import isCoprimeInt

def kGen (P):
    """
        return random integer k which is coprime to P-1, 
        and k1 = k**(-1) (k*k1 mod p-1 = 1), (1 < k < P-1)
        or 0 if there is no k coprime to P-1 
    """
    listOfCheckedK = list (range (2, P - 2))
    length = P - 4
    index = random.randint (0, length)
    k = listOfCheckedK.pop(index)
    length -= 1
    
    while True:
        if length <= 0:
            return 0, 0    # k is not exist
        index = random.randint (0, length)
        k = listOfCheckedK.pop(index)
        length -= 1
        
        k1 = isCoprimeInt (k, P - 1)
        if (k1):
            if (k1 < 0): k1 += P - 1     
            return k, k1
###################################################
class ElGamDS:
    
    def __init__ (self):
#        self.__p = P
#        self.__g = G  
#        self.__x = random.randint (2, p-2)
        pass
    #--------------------------
    def makeDS (self, taskFname, xKeyFnmae):
        # get keys, all variables as in the book
        xKey = open (xKeyFnmae, 'rb')
        p = xKey.read (4)
        g = xKey.read (4)
        x = xKey.read (4)
        y = xKey.read (4)
        xKey.close()
        p = int.from_bytes (p, byteorder='little')
        g = int.from_bytes (g, byteorder='little')
        x = int.from_bytes (x, byteorder='little')
        y = int.from_bytes (y, byteorder='little')
#        print (p, g, x, y)
        h = RSA_ds.getFileHash (taskFname)  # h > p !!!!!!!!!!!!!!!!!!!!!!!!!!
        k, k1 = kGen (p)
        r = binPow (g, k, p)        # 4.5
        u = (h - x * r) % (p - 1)   # 4.6
        s = k1 * u % (p - 1)        # 4.7
        print('h, k, k1, r, u, s =', h, k, k1, r, u, s)
        return r, s, g, p, y     #   4.9
    #--------------------------
    def readDS (self, taskFname, r, s, g, p, y):
        #   (y**r)*(r**s) = (g**h) % p  4.10    !!!! 
        h =  RSA_ds.getFileHash (taskFname)
        left = ((y**r) * (r**s)) % p  #   mod p ?????
        right = binPow (g, h, p)
        
        print ('left =' ,left, '; right =',right)
        if (left == right):
            return True
        return False
###################################################
if __name__ == '__main__':

#    p, g = genPublicKeys (1000)
    A = ElGamDS()
    B = ElGamDS()
    r, s, g, p, y = A.makeDS ('AliceDoc.txt', 'ElGam_AliceKeys_pgcd_4')        
    flag = B.readDS ('AliceDoc.txt', r, s, g, p, y)
    
    if flag:
        print ('signature is valid')
    else:
        print ('signature is invalid')        
#    print(kGen(347))