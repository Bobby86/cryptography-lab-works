# -*- coding: utf-8 -*-
"""
Created on Fri Oct 24 15:57:53 2014

@author: V.Rybnikov IS-16
"""
import sys
import os

PACKAGE_PARENT = '..'
SCRIPT_DIR = os.path.dirname(os.path.realpath(os.path.join(os.getcwd(), os.path.expanduser(__file__))))
sys.path.append(os.path.normpath(os.path.join(SCRIPT_DIR, PACKAGE_PARENT)))

import random
import hashlib
from lab01.BinPow import binPow
from ctypes import *
import RSA_ds
from lab02.Shamir import isCoprimeInt
from lab01.DiffieHellman import *

###################################################    
def genQP ():
    """
        q is 254 to 256 bit random simple integer
        p is 1022 to 1024 bit simple integer
        (p = b * q + 1 ) and (b = 2**(1024-256) = 2 ** 768), 768 - count of << 
        so we got 256 bit int multiplied to b => 1024 bit int
        q definitely bigger than md5 hash
    """    
    i = 0
    checkedQ = []

    for i in range(1000001):    # 1mln and 1 try
        q = random.randint ((1<<254), (1<<256))
        i += 1
        if q in checkedQ:
            continue
        checkedQ.append (q)
        if isSimple (q):
            # TODO b = random
            p = (q << 768) + 1  # equal to (q * (2 ** 768) + 1)
            print(q)
            if isSimple (p):
#                print('q = ', q)
#                print('p = ', p)
                print ('done')
                break
        if i == 1000000:    #  exit after 1mln trys
            print ('cannot generate q and p try again')
            q, p = 0, 0
    return q, p

def genA (q, p):
    """
        return a: a**q mod p = 1
    """
    gList = []
    a = 1
    while a == 1:
        g = random.randint (1, 10001)
        if g in gList:
            continue
        gList.append (g)
        a = binPow (g, int((p - 1) / q), p)     # ??????????????????? float
    print ('a validation: ', binPow(a, q, p))   # == 1
    return a
###################################################
class Abonent:
    def __init__(self, p, q, a):
        self.__p = p
        self.__q = q
        self.__a = a
        self.__x = random.randint (1, q - 1)    #   secret key
        self.__y = binPow (a, self.__x, p)      #   shared key
    #-------------------        
    def __packKeys (self):
        #   save the keys   (in case of BIG numbers convert it to string)
        strX = str (self.__x)
        strY = str (self.__y) + '\n'
        strQ = str (self.__q) + '\n'
        strP = str (self.__p) + '\n'
        strA = str (self.__a)
        sharedKeys = open ('DSA_sharedKeys_yqpa', 'w')   
        secretKey = open ('DSA_secretKey_x_4', 'w')         #   for Alice only
        secretKey.write (strX)
        sharedKeys.write (strY)
        sharedKeys.write (strQ)
        sharedKeys.write (strP)
        sharedKeys.write (strA)
        sharedKeys.close ()
        secretKey.close ()
    #-------------------        
    def __unpackKeys (self):       
        l = []
        sharedKeys = open ('DSA_sharedKeys_yqpa', 'r')
        for line in sharedKeys:
            key = int (line.strip())
            l.append (key)
        return l[0], l[1], l[2], l[3]   #   y, q, p, a
    #-------------------
    def makeDS (self, targetFname):
        print ('#### Making Digital Signature #####')        
        h = RSA_ds.getFileHash (targetFname)
        print ('hash =', h)
        r = 0
        # calculation of r and s
        while r == 0:
            k = random.randint (1, self.__q - 1)
            r = (binPow (self.__a, k, self.__p)) % self.__q
            if r == 0:
                continue
            else:
                s = (k * h + self.__x * r) % self.__q
                if s < 0:
                    s += self.__q
                if s:
                    break
        print ('r, s =', r, s)
        self.__packKeys ()
        print (self.__y )
        print (self.__q)
        print (self.__p)
        print (self.__a)
        
        return r, s
    #-------------------        
    def readDS (self, targetFname, r, s):
        
        print ('#### Checking Digital Signature #####')        
        y, q, p, a = self.__unpackKeys ()       
        print ('unpacked keys y, q, p, a')
        print (y)
        print (q)
        print (p)
        print (a)
        print ('r, s =', r, s)
        if (r < 1 or s < 1 or r > self.__q or s > self.__q):
            print ('invalid arguments (r, s)')
            return False
        h = RSA_ds.getFileHash (targetFname)
        print ('hash =', h)
        # getting h^-1
        h1 = isCoprimeInt (h, q)    #   ?????????????????
        if (h1):
            if (h1 < 0): h1 += q    
        else:
            print ('cannot get h^-1')
            return False
        print('h^-1: ', (h1 * h) % q)
        u1 = (s * h1) % q
        print('u1 =', u1)
        u2 = ((-r) * h1) % q
        if u2 < 0:
            u2 += q
        print('u2 =', u2)
        # calculate v = ((a ** u1)*(y ** u2) mod p) mod q
        v1 = binPow (a, u1, p)
        v2 = binPow (y, u2, p)
#        print ('v1 =', v1)
#        print ('v2 =', v2)
        v = ((v1 * v2) % p) % q
        
        print('v =', v)
        if v == r:
            return True
        else:
            return False        
###################################################
if __name__ == '__main__':
    
#    q, p = genQP()
#    print (q)
#    print ()
#    print (p)    
#    print ()
    q = 36431760645058165820445233533934403165025087034583900023846885580948999630397
    p = 56560967535821748777755187678206841921734900518834833361567997311248512224364865737275724696197006159162364654752631570540068295606086361293749175527185942602708509918298817253818695482237502738464894724385729242583435984782932228833461835965918076658226328543177792738466514898942229986714679674367568248833
    a = genA(q, p)
    print('a =', a)

    A = Abonent(p, q, a)    
    B = Abonent(p, q, a)        
    
    r, s = A.makeDS ('AliceDoc.txt')
    flag = B.readDS ('AliceDoc.txt', r, s)
    
    if flag:
        print ('signature is valid')
    else:
        print ('signature is invalid')      
    
    
#        x = 82774056038680829345636852965705695348736113624791436994687278782468822056005876586455393626590546970637535492362120649515611907906989913177364294645245337042492962196534341670417721866556981058848981801900410171187232513168975134934550719029537928239864610991620145699580435993114922604453506315115042963457
#        s = str (x)
#        s += '\n'
#        b = '1284189234\n'
#        secretKey = open ('DSA_secretKey_x_4', 'w')
#        secretKey.write (s)
#        secretKey.write (b)
#        secretKey.close ()
#        secretKey = open ('DSA_secretKey_x_4', 'r')
#        l = []
#        for line in secretKey:
#            l.append (line)
#        sr = l[0].strip()
#        sb = l[1].strip()
#        secretKey.close ()
#        print (sr)
#        print (sb)
#        xr = int (sr.strip())
#        xb = int (sb.strip())
#        print (xr)
#        print (sb)
