# -*- coding: utf-8 -*-
"""
Created on Thu Oct 23 13:00:15 2014

@author: V.Rybnikov IS-16
"""

import sys
import os

PACKAGE_PARENT = '..'
SCRIPT_DIR = os.path.dirname(os.path.realpath(os.path.join(os.getcwd(), os.path.expanduser(__file__))))
sys.path.append(os.path.normpath(os.path.join(SCRIPT_DIR, PACKAGE_PARENT)))

import hashlib
from lab01.BinPow import binPow
from ctypes import c_uint8, c_uint32, c_uint16

def getFileHash (taskFname):
    # hashing file
    BLOCKSIZE = 65536
    hasher = hashlib.md5 ()
    file = open (taskFname, 'rb')   
    buffer = file.read (BLOCKSIZE)
    while len(buffer) > 0:
        hasher.update (buffer)
        buffer = file.read (BLOCKSIZE)
    y = hasher.digest ()
    print (y)
    file.close ()
    y = int.from_bytes (y, byteorder='little')
    print(y)
    return y     
#################################################
class RSAdigitalSignature:
    
    def __init__(self):
        pass
    
    #---------------------
    def makeNewDS (self, taskFname, sharedKeysFname, secretKeysFname):
        print ('####### Alice make digital signature #########')
        # Alice read secret key
        secretKey = open (secretKeysFname, 'rb')  # 32 bit c
        c = secretKey.read (4)
        secretKey.close()
        c = int.from_bytes (c, byteorder='little')
        print (c)
        # and shared keys N, d
        sharedKeys = open (sharedKeysFname, 'rb') # 32 bit N + 32 bit d
        N = sharedKeys.read(4)  # TODO
        d = sharedKeys.read(4)
        sharedKeys.close ()
        N = int.from_bytes (N, byteorder='little')
        d = int.from_bytes (d, byteorder='little')        
        # build digital signature and return it and N, d
        y = getFileHash (taskFname)
        s = binPow (y, c, N)
        print('Alice signature:', s)
        return s, N, d
    #---------------------
    def readDS (self, taskFname, s, N, d):   #   Files names
        print ('####### Bob check signature #########')
#        #   read shared keys
#        sharedKeys = open (sharedKeysFname, 'rb') # 32 bit N + 32 bit d
#        N = sharedKeys.read(4)
#        d = sharedKeys.read(4)
#        sharedKeys.close ()
#        N = int.from_bytes (N, byteorder='little')
#        d = int.from_bytes (d, byteorder='little')        
        #   check hash
        check = getFileHash (taskFname)
#        print ('readed hash:', check)
        w = binPow (s, d, N)
        print ('calculated signature:', w)
        return (check == w)
###################################################
if __name__ == '__main__':

    A = RSAdigitalSignature()
    As, AN, Ad = A.makeNewDS ('AliceDoc.txt', 'RSA_sharedKeys', 'RSA_secretKeys')
    print ('shared keys d and N:', Ad, AN)
    B = RSAdigitalSignature()
    result = B.readDS ('AliceDoc.txt', As, AN, Ad)
    if result:
        print ('signature is valid')
    else:
        print ('signature is invalid')
        
#    f = binPow (15584351167096156993034647368579262412, 1399, 1591)
#    print(f)
#    f = binPow (f, 1231, 1591)
#    print(f)
