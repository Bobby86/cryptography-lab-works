# -*- coding: utf-8 -*-
"""
Created on Fri Sep 26 13:18:44 2014

@author: V.Rybnikov
"""

import sys
import os

PACKAGE_PARENT = '..'
SCRIPT_DIR = os.path.dirname(os.path.realpath(os.path.join(os.getcwd(), os.path.expanduser(__file__))))
sys.path.append(os.path.normpath(os.path.join(SCRIPT_DIR, PACKAGE_PARENT)))

from random import randint
from lab01.Evclid import gcd
from lab01.BinPow import binPow
from lab01.DiffieHellman import *
from ctypes import *
#################################################
def isCoprimeInt (a, b):    # взаимнопростые
    """
        Взаимнопростые числа: да - True, нет - False
    """
    
    if ((a == b) or (0 == a) or (0 == b)): return False
    if ((1 == a) or (1 == b)): return True    
    
    if (b > a): 
        a += b
        b = a - b
        a = a - b
    
    res = gcd(a, b)
    if (1 == res[0]):
        return res[2]
    
    return False
#################################################
def parseInt (int32):
    """
        parse int32 to 4x int with each byte from int32 separately
    """
    first = int32 & 255
    second = (int32 & (255 << 8)) >> 8
    third = (int32 & (255 << 16)) >> 16
    fourth = (int32 & (255 << 24)) >> 24
    
#    print (first, second, third, fourth)
    return [first, second, third, fourth]
#################################################
class Person:
    
    def __init__ (self, P):
        self.__p = P
        self.__listOfC = list (range (3, P - 2, 2)) 
        self.__c = 0
        self.__d = 0
    #-----------------        
    # genering of simple number (Ci < p), (cA, dA, cB, dB); private method
    def __genCD (self):   
                
        length = len (self.__listOfC)
        if (0 == length):
            return False
        
        while (True):

            self.__c =  self.__listOfC.pop (randint (0, length - 1))
            length -= 1
            
            res = isCoprimeInt (self.__p - 1, self.__c)
            if (res):
                #   calculate d (c*d mod p = 1)
                if (self.__d < 0):
                    self.__d += self.__p - 1
                else:
                    self.__d = self.__p - 1 - res
                print ('c =', self.__c)
                print ('d =', self.__d)
                break
            
            if (0 == length):
                return False                            
    #-----------------        
    def getX1 (self, msgOrX1FileName, encFnameX1orX2, msgFlag = False):
    
#        if (nBytes not in [1, 2]):  #   read 1 byte for X1 from original message
#            return False            #   or 2 bytes for X2
        nBytes = 2      # 4 bytes will be readed for X2 encoding from X1
        if msgFlag:
            nBytes = 1  # 1 byte will be readed for X1 encoding from original message
            
        self.__genCD()
#        nBytes = 1  # 1 byte will be readed per ation
        msg = open (msgOrX1FileName, 'rb')  
        enc = open (encFnameX1orX2, 'wb')
        msgData = msg.read (nBytes)
        print (msgData)
        flag = True
        while (msgData):
            
            msgData = int.from_bytes (msgData, byteorder='little')
            if flag: 
                print ('inc', msgData)

            msgData = binPow (msgData, self.__c, self.__p)
                
            if flag: 
                print (self.__c)
                print ('enc', msgData)
                flag = False
            if msgFlag:
                msgData = bytes(c_uint16 (msgData)) #   32 bit uint convertation 
            else:
                msgData = bytes(c_uint32 (msgData)) #   32 bit uint convertation
            enc.write (msgData) #   4 bytes
            msgData = msg.read (nBytes) #   read next n bytes
            
        msg.close()
        enc.close()                
    #-----------------        
    def getX2 (self, X2orX3FileName, encFnameX3orX4, decodeFlag = False): 

        nBytes = 4
        if decodeFlag:
            nBytes = 8
        msg = open (X2orX3FileName, 'rb')  
        enc = open (encFnameX3orX4, 'wb')
        msgData = msg.read (nBytes)
        print (msgData)
        flag = True
        while (msgData):
            
            msgData = int.from_bytes (msgData, byteorder='little')
            if flag: 
                print ('inc', msgData)
            
            msgData = binPow (msgData, self.__d, self.__p)
            if flag: 
                print (self.__d)
                print ('enc', msgData)
                flag = False

            if decodeFlag:
                msgData = bytes(c_uint8 (msgData)) 
            else:
                msgData = bytes(c_uint64 (msgData)) 
            
            enc.write (msgData) #   4 bytes for X3 or 1 byte for X4
            msgData = msg.read (nBytes) #   read next n bytes
            
        msg.close()
        enc.close()                
    #-----------------        
#################################################
if __name__ == '__main__':

    p = 31
    
    A = Person (p)
    B = Person (p)

#    parseInt (134480400)
    
    print('p =', p)
    
    print ('=====MSG====')
    A.getX1 ('readme.txt', 'shamir_x1', True)           # message to X1
    print ('=====X2=====')
    B.getX1 ('shamir_x1', 'shamir_x2')                  # X1 to X2
    print ('=====X3=====')
    A.getX2 ('shamir_x2', 'shamir_x3')                  # X2 to X3    
    print ('=====X4=====')
    B.getX2 ('shamir_x3', 'shamir_decoded.txt', True)   # X3 to X4=message
    