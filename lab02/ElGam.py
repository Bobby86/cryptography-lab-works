# -*- coding: utf-8 -*-
"""
Created on Sun Sep 28 13:38:53 2014

@author: V.Rybnikov IS-16
"""
import sys
import os

PACKAGE_PARENT = '..'
SCRIPT_DIR = os.path.dirname(os.path.realpath(os.path.join(os.getcwd(), os.path.expanduser(__file__))))
sys.path.append(os.path.normpath(os.path.join(SCRIPT_DIR, PACKAGE_PARENT)))

import random
from lab01.BinPow import binPow
from lab01.DiffieHellman import *
from ctypes import *
#################################################
class Abonent:
    #-----------------            
    def __init__(self, P, G):
        
        self.__p = P
        self.__g = G
#        self.__m = M
        self.__c = random.randint(2, P-2)   # secret key
        self.__d = binPow (G, self.__c, P)  # shared key
    #-----------------        
    def __calcRE (self, dB, msg):
        
        k = random.randint (1, self.__p - 2)
        r = binPow (self.__g, k, self.__p)
        e = binPow (dB, k, self.__p)
        e = (msg * e) % self.__p
        
        return r, e, k
    #-----------------        
    def getD (self):
        
        return self.__d
    #-----------------        
    def __calcM (self, r, e):
        
        m = binPow (r, (self.__p - 1 - self.__c), self.__p)
        m = (e * m) % self.__p

        return m
    #-----------------        
    def encode (self, msgFname, encFname, dB):        
        
        nBytes = 1  # 1 byte will be readed per iteration
        msg = open (msgFname, 'rb')  
        enc = open (encFname, 'wb')
        # save p, g, c, d for digital signature (Alice part)
        xKeyFile = open ('ElGam_AliceKeys_pgcd_4', 'wb')    # c = x in the book
        xKeyFile.write (c_uint32 (self.__p))
        xKeyFile.write (c_uint32 (self.__g))
        xKeyFile.write (c_uint32 (self.__c))
        xKeyFile.write (c_uint32 (self.__d))
        xKeyFile.close ()
        
        msgData = msg.read (nBytes)
#        flag = True
        while (msgData):
            
            msgData = int.from_bytes (msgData, byteorder='little')
            
            r, e, k = self.__calcRE (dB, msgData)
#            print('r, e =', r, e)
            r = bytes(c_uint16 (r)) #   16 bit uint convertation / +2 byte
            e = bytes(c_uint16 (e)) #   + 2 byte
            
            enc.write (r)
            enc.write (e)
#            xKeyFile.write (bytes (c_uint32 (k)))  # 4 bytes for single k
            msgData = msg.read (nBytes) #   read next n bytes
            
        msg.close()
        enc.close()        
    #-----------------        
    def decode (self, encFname, decFname):        

        nBytes = 2  #  number of bytes will be readed per iteration
        enc = open (encFname, 'rb')  
        dec = open (decFname, 'wb')
        r = enc.read (nBytes)
        e = enc.read (nBytes)

        while (r and e):
            
            r = int.from_bytes (r, byteorder='little')
            e = int.from_bytes (e, byteorder='little')
#            print('r, e =', r, e)
            m = self.__calcM (r, e)            
#            print('m =', m)
            m = bytes(c_uint8 (m)) #   1 byte            
            dec.write (m)
            r = enc.read (nBytes)
            e = enc.read (nBytes)
            
        dec.close()
        enc.close()             
###################################################
if __name__ == '__main__':
    
    p, g = genPublicKeys (1000)
#    m = random.randint (1, p-1)
    print ('p, g =', p, g)
    
    A = Abonent (p, g)
    B = Abonent (p, g)
    
    dB = B.getD()
    A.encode ('readme.txt', 'ElGam_encoded', dB)
    B.decode ('ElGam_encoded', 'ElGam_decoded.txt')
