# -*- coding: utf-8 -*-
"""
Created on Mon Sep 29 12:37:52 2014

@author: V.Rybnikov IS-16
"""
import sys
import os

PACKAGE_PARENT = '..'
SCRIPT_DIR = os.path.dirname(os.path.realpath(os.path.join(os.getcwd(), os.path.expanduser(__file__))))
sys.path.append(os.path.normpath(os.path.join(SCRIPT_DIR, PACKAGE_PARENT)))

from ctypes import c_uint8

#################################################
def encoding (msgFname, keyFname, encFname):
    
    nBytes = 1  #   read 8 bit
    msg = open (msgFname, 'rb')
    key = open (keyFname, 'rb')
    enc = open (encFname, 'wb')

    msgData = msg.read (nBytes)
    keyData = key.read (nBytes)
    while (msgData and keyData):

        msgData = int.from_bytes (msgData, byteorder='little') # convertion of byte to int
        keyData = int.from_bytes (keyData, byteorder='little')
        encodedData = (msgData) ^ (keyData)   # encoding
#        print ('msg', msgData)
#        print ('key', keyData)
#        print ('enc', encodedData)
        encodedData = c_uint8 (encodedData) #   convertion to short uint
        enc.write (bytes(encodedData))  # write encoded byte to output file
        msgData = msg.read (nBytes)     # read next byte
        keyData = key.read (nBytes)

    msg.close()
    key.close()
    enc.close()
#################################################
def decoding (encFname, keyFname, decFname):
    
    nBytes = 1  #   read 8 bit
    msg = open (decFname, 'wb')
    key = open (keyFname, 'rb')
    enc = open (encFname, 'rb')
    
    encData = enc.read (nBytes)
    keyData = key.read (nBytes)
    while (encData and keyData):

        encData = int.from_bytes (encData, byteorder='little')
        keyData = int.from_bytes (keyData, byteorder='little')
        decodedData = int(encData) ^ int(keyData)   # decoding
#        print ('enc', encData)
#        print ('key', keyData)
#        print ('dec', decodedData)
        decodedData = c_uint8 (decodedData)  #   convertion to short uint
        msg.write (bytes(decodedData))  # write decoded byte to output file
        encData = enc.read (nBytes)     # read next byte
        keyData = key.read (nBytes)

    msg.close()
    key.close()
    enc.close()    
#################################################
if __name__ == '__main__':
    
    encoding ('readme.txt', 'setup.exe', 'vernam_encoded')
    decoding ('vernam_encoded', 'setup.exe', 'vernam_decoded.txt')
    
    