# -*- coding: utf-8 -*-
"""
Created on Mon Sep 29 14:39:25 2014

@author: V.Rybnikov IS-16
"""
import sys
import os

PACKAGE_PARENT = '..'
SCRIPT_DIR = os.path.dirname(os.path.realpath(os.path.join(os.getcwd(), os.path.expanduser(__file__))))
sys.path.append(os.path.normpath(os.path.join(SCRIPT_DIR, PACKAGE_PARENT)))

import random
from lab01.Evclid import gcd
from lab01.BinPow import binPow
from lab01.DiffieHellman import *
from lab02.Shamir import isCoprimeInt
from ctypes import c_uint8, c_uint32, c_uint16
#################################################
def getRandomIndex (List):
    """
    Return random index list 
        (from second half of list if it longest than 3 elements)
    """
    length = len (List)
    index = 0
    if (length < 4): index = random.randint (0, length-1)
    else: index = random.randint (int (length/2), length-1) # biggest values
    
    return index
#################################################
def getDC (fi):
    """
    Return d and c numbers, 
        where (d * c mod fi = 1), by emproved algorythm of Evclid
    """    
    dList = list (range (1, fi - 1))
    length = len(dList)  

    if (length < 2):
        return 0, 0
        
    while (True):
        
        d = dList.pop (random.randint (0, length-1))
        length -= 1

        c = isCoprimeInt (fi, d)
        if (c):
            if (c < 0): c += fi
            return d, c

        if (0 == length):
            return 0, 0
#################################################
class Abonent:
    #-----------------            
    def __init__(self, maxP):
        
        simplesList = sieveOfEratosthenes (maxP)
        index = getRandomIndex (simplesList)
        self.__P = simplesList.pop (index)
        index = getRandomIndex (simplesList) 
        self.__Q = simplesList.pop (index)
        self.__N = self.__P * self.__Q
        self.__fi = (self.__P - 1) * (self.__Q - 1)
        self.__d, self.__c = getDC (self.__fi)
        self.__Nb = 0
        self.__db = 0
        
#        print ('P =', self.__P)
#        print ('Q =', self.__Q)
#        print ('N =', self.__N)
#        print ('fi =', self.__fi)
#        print ('d =', self.__d)
#        print ('c =', self.__c)
#        print ( (self.__c * self.__d) % self.__fi )    # == 1
    #-----------------            
    def setNbdb (self, Nb, db):
        self.__Nb = Nb  # received N from another abonent
        self.__db = db   
        return
    #-----------------            
    def getN (self):
        return self.__N #   shared
    #-----------------            
    def getD (self):
        return self.__d #   shared
    #-----------------            
    def encodeMsg (self, msgFname, encFname):
        
        if (not self.__Nb or not self.__db):
            return 0
            
    #   save the keys    32bit each (as Alice, for digital signatures)
        print ('N =', self.__N)
        print ('d =', self.__d)
        print ('c =', self.__c)            
#        saveN = bytes(c_uint32 (self.__N))
#        saveD = bytes(c_uint32 (self.__d))
#        saveC = bytes(c_uint32 (self.__c))
        sharedKeysFile = open ('RSA_sharedKeys', 'wb')
        secretKeysFile = open ('RSA_secretKeys', 'wb')
        sharedKeysFile.write (c_uint32 (self.__N))
        sharedKeysFile.write (c_uint32 (self.__d))
        secretKeysFile.write (c_uint32 (self.__c))
        sharedKeysFile.close ()
        secretKeysFile.close ()
        print ('keys has been saved successful')
#        sharedKeys = open ('RSA_sharedKeys', 'rb') # 32 bit N + 32 bit d
#        secretKey = open ('RSA_secretKeys', 'rb')  # 32 bit c
#        N = sharedKeys.read(4)
#        d = sharedKeys.read(4)
#        c = secretKey.read (4)
#        print (N, d, c)
#        sharedKeys.close ()
#        secretKey.close()        
#        N = int.from_bytes (N, byteorder='little')
#        d = int.from_bytes (d, byteorder='little')
#        c = int.from_bytes (c, byteorder='little')
#
#        print (N, d, c)
#        
        nBytes = 1  # 1 byte will be readed per iteration
        msg = open (msgFname, 'rb')  
        enc = open (encFname, 'wb')
        msgData = msg.read (nBytes)        
#        flag = True

        while (msgData):
            
            msgData = int.from_bytes (msgData, byteorder='little')
#            if flag: 
#                print ('message:', msgData)
            encodedData = binPow (msgData, self.__db, self.__Nb)  # encoding
#            if flag: 
#                print ('encoded:', encodedData)
            encodedData = bytes(c_uint32 (encodedData)) #   convertion to uint
#            if flag: 
#                print ('encPack:', encodedData)
#                flag = False
            enc.write (encodedData)  # write encoded byte to output file
            msgData = msg.read (nBytes)     # read next byte
                 
        msg.close()
        enc.close()
    #-----------------            
    def decodeMsg (self, encFname, decFname):
        
        nBytes = 4 #  number of bytes will be readed per iteration
        enc = open (encFname, 'rb')  
        dec = open (decFname, 'wb')
        encData = enc.read (nBytes)
#        flag = True

        while (encData):
            
            encData = int.from_bytes (encData, byteorder='little')
#            if flag: 
#                print ('\'encoded:', encData)
            decData = binPow (encData, self.__c, self.__N)  # decoding
#            if flag: 
#                print ('decoded:', decData)
            decData = bytes(c_uint8 (decData)) #   convertion to short uint
#            if flag: 
#                print ('decPack:', decData)
#                flag = False
            dec.write (decData)  # write encoded byte to output file
            encData = enc.read (nBytes) # read next byte
                    
        dec.close()
        enc.close()
    #-----------------  private method saves c, db, Nb
#    def __saveKeys (self, sharedKeysFilename, secretKeyFilename):
#        pass
#    #-----------------            
#    def makeSignature (self, fname):
#        pass
#        
###################################################
if __name__ == '__main__':

#    print(isCoprimeInt(11, 7))
        maxPQ = 50    #   maximum value of P or Q
        
        A = Abonent (maxPQ)
        B = Abonent (maxPQ)
        
        Nb = B.getN ()
        db = B.getD ()
        A.setNbdb (Nb, db)
        print ('A get Nb from B:       ', Nb)
        print ('A get db from B:       ', db)
        
        A.encodeMsg ('readme.txt', 'RSA_encoded')
#        print ('A sent encoded message:', encoded)
        B.decodeMsg ('RSA_encoded', 'RSA_decoded.txt')
#        print ('B decode message:      ', decoded)
