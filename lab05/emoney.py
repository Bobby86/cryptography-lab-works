# -*- coding: utf-8 -*-
"""
Created on Thu Sep  4 18:51:44 2014

@author: V.Rybnikov IS-16
"""

import sys
import os

PACKAGE_PARENT = '..'
SCRIPT_DIR = os.path.dirname(os.path.realpath(os.path.join(os.getcwd(), os.path.expanduser(__file__))))
sys.path.append(os.path.normpath(os.path.join(SCRIPT_DIR, PACKAGE_PARENT)))

import random
import hashlib
from lab01.BinPow import binPow
from ctypes import *
from lab02.RSA import getDC
from lab02.Shamir import isCoprimeInt
from lab01.DiffieHellman import *
from lab04.MentalPocker import *    # <- genBigP
#######################################
class Bank:
    
    def __init__(self):
        
        self.__P = genBigP ((1 << 255) + 1, (1 << 256) - 1)
        self.__Q = genBigP ((1 << 255) + 1, (1 << 256) - 1)
        #print (self.__P)#
        self.__N = self.__P * self.__Q
        print ('P:', self.__P)
        print ('Q:', self.__Q)
        print ('N:', self.__N)
        #print(self.__N)#
        self.__rates = {10:[], 20:[], 50:[], 100:[]}    # note rate : [s]
        self.__notes = {}                   # note rate : [d, c]
        #   d is shared note rate and c is secret key fpr 10, 20, 50, 100$        
        for rate in self.__rates.keys():
            c, d = genSecretKeys ((self.__P - 1) * (self.__Q - 1))
            self.__notes[rate] = [d, c]
            print (rate,'$ < c , d >: <', c, ',', d, '>')
            #print ((d*c)%((self.__P - 1) * (self.__Q - 1)))#
        #print (self.__noteRate)#
    #---------------------------------
    def checkRate (self, rate):
        return (rate in self.__rates.keys())        
    #---------------------------------
    def getN (self):
        return self.__N
    #---------------------------------
    def getD (self, rate):
        if not self.checkRate (rate): return 0
        return self.__notes[rate][0]
    #---------------------------------
    def blindSign (self, buyerNote, rate):
        if not self.checkRate (rate): return 0
        s = binPow (buyerNote, self.__notes[rate][1], self.__N)
        self.__rates[rate].append (s)
        return s
    #---------------------------------
    def checkNote (self, s, rate):
        if not self.checkRate (rate): return 0
        if s in self.__rates[rate]: 
            # Bank check validation of the note (remove money from client account and put it to sellers account)
            # TODO remove note from list? 
            return True
#######################################
class Buyer:

    def __init__(self):
        self.__cash = [[], [], [], []]    #   rate : n    #TODO future
        self.__note = 0                   # for now
    #---------------------------------
    def genNote (self, bank, rate):
        N = bank.getN ()
        n = random.randint ((N >> 1), (N - 1))
        print ('generated random n:', n)
        #print (n)#
        hasher = hashlib.md5 ()
        hasher.update (bytes(str(n), 'utf-8'))
        n = hasher.digest ()
        n = int.from_bytes (n, byteorder='little')  # n hashed by md5
        print()
        input ('Press Enter for the next step')
        print()


        r = random.randint ((N >> 1), (N - 1))
        print ('r:', r)
        d = bank.getD (rate)
        r = binPow (r, d, N)
        print ('encoded r:', r)
        n = (n * r)% N
        
        print ('(r * md5(n) mod N) will be sent to Bank:', n)
        self.__n = n
        s = bank.blindSign (n, rate)
        if not s:
            return 0
        print()
        input ('Press Enter for the next step')
        print()
        print ('Bank got n=', n, 'note from a Client and check a rate', rate,'of this note')
        print ('Bank made blind signature for n, add it to valid notes specified by the rate')
        print ('And send signaured n to a Client-> s:', n)
        return s
    #---------------------------------

#######################################
if __name__ == '__main__':
    
    print ('--BANK_INIT--')
    sber = Bank ()
    print()
    input ('Press Enter for the next step')
    print()
    print ('--Client part--')
    vasya = Buyer ()

    rate = input ('Client want to get some note from the Bank of rate: ')
    rate = int(rate)
    s = vasya.genNote (sber, rate)
    if not s:
        print ('the Bank: rate of the note is unavalable')
        sys.exit(1)
    
    print ('Client got signed s=', s,'from the Bank and give it to the seller')
    print()
    input ('Press Enter for the next step')
    print()
    print ('Seller got note s=', s,'with the rate of', rate,'from a buer and send it to the Bank to ensure it is illegable valid note')

    seller = sber.checkNote (s, rate)
    if not seller:
        print ('the Bank: rate of the note is unavalable')
        sys.exit(1)

    print()
    input ('Press Enter for the next step')
    print()

    print ('the Bank check s in the database and tell to a seller:')
    if seller:
        print ('note is valid')
        print ('the Bank add value of note\'s rate to a seller\'s account')
    else:
        print ('note is not valid')        
    
